Source: tomcat6
Section: java
Priority: optional
Maintainer: Debian Java Maintainers <pkg-java-maintainers@lists.alioth.debian.org>
Uploaders: Torsten Werner <twerner@debian.org>,
 Ludovic Claude <ludovic.claude@laposte.net>,
 Damien Raude-Morvan <drazzib@debian.org>,
 Miguel Landaeta <miguel@miguel.cc>,
 tony mancill <tmancill@debian.org>,
 Emmanuel Bourg <ebourg@apache.org>
Build-Depends: default-jdk, ant-optional, debhelper (>= 9), po-debconf
Build-Depends-Indep: maven-repo-helper (>> 1.0.1), libecj-java
Standards-Version: 3.9.7
Vcs-Git: https://anonscm.debian.org/git/pkg-java/tomcat6.git
Vcs-Browser: https://anonscm.debian.org/cgit/pkg-java/tomcat6.git
Homepage: http://tomcat.apache.org

#Package: tomcat6-common
#Architecture: all
#Depends: libtomcat6-java (>= ${source:Version}), ${misc:Depends},
# default-jre-headless | java7-runtime-headless | java7-runtime | java6-runtime-headless | java6-runtime | java5-runtime
#Description: Servlet and JSP engine -- common files
# Apache Tomcat implements the Java Servlet and the JavaServer Pages (JSP)
# specifications from Sun Microsystems, and provides a "pure Java" HTTP web
# server environment for Java code to run.
# .
# This package contains common files needed by the tomcat6 and tomcat6-user
# packages (Tomcat 6 scripts and libraries).

#Package: tomcat6
#Architecture: all
#Depends: tomcat6-common (>= ${source:Version}), ucf,
# adduser, ${misc:Depends}
#Recommends: authbind
#Suggests: tomcat6-docs (>= ${source:Version}),
# tomcat6-admin (>= ${source:Version}),
# tomcat6-examples (>= ${source:Version}),
# tomcat6-user (>= ${source:Version}),
# libtcnative-1 (>= 1.1.30)
#Description: Servlet and JSP engine
# Apache Tomcat implements the Java Servlet and the JavaServer Pages (JSP)
# specifications from Sun Microsystems, and provides a "pure Java" HTTP web
# server environment for Java code to run.
# .
# This package contains only the startup scripts for the system-wide daemon.
# No documentation or web applications are included here, please install
# the tomcat6-docs and tomcat6-examples packages if you want them.
# Install the authbind package if you need to use Tomcat on ports 1-1023.
# Install tomcat6-user instead of this package if you don't want Tomcat to
# start as a service.

#Package: tomcat6-user
#Architecture: all
#Depends: tomcat6-common (>= ${source:Version}), netcat, ${misc:Depends}
#Suggests: tomcat6-docs (>= ${source:Version}),
# tomcat6-admin (>= ${source:Version}),
# tomcat6-examples (>= ${source:Version}),
# tomcat6 (>= ${source:Version})
#Description: Servlet and JSP engine -- tools to create user instances
# Apache Tomcat implements the Java Servlet and the JavaServer Pages (JSP)
# specifications from Sun Microsystems, and provides a "pure Java" HTTP web
# server environment for Java code to run.
# .
# This package contains files needed to create a user Tomcat instance.
# This user Tomcat instance can be started and stopped using the scripts
# provided in the Tomcat instance directory.

#Package: libtomcat6-java
#Architecture: all
#Depends: libecj-java,
#         libcommons-dbcp-java,
#         libcommons-pool-java,
#         libservlet2.5-java (>= ${source:Version}), ${misc:Depends}
#Suggests: tomcat6 (>= ${source:Version})
#Conflicts: tomcat6-common (<< 6.0.20-5)
#Description: Servlet and JSP engine -- core libraries
# Apache Tomcat implements the Java Servlet and the JavaServer Pages (JSP)
# specifications from Sun Microsystems, and provides a "pure Java" HTTP web
# server environment for Java code to run.
# .
# This package contains the Tomcat core classes which can be used by other
# Java applications to embed Tomcat.

#Package: libservlet2.4-java
#Section: oldlibs
#Priority: extra
#Architecture: all
#Depends: ${misc:Depends}, libservlet2.5-java
#Description: Transitional package for libservlet2.5-java
# This is a transitional package to facilitate upgrading from
# libservlet2.4-java to libservlet2.5-java, and can be safely
# removed after the installation is complete.

Package: libservlet2.5-java
Architecture: all
Depends: ${misc:Depends}
Replaces: libservlet2.4-java (<= 5.5.33-1)
Conflicts: libservlet2.4-java (<= 5.5.33-1)
Description: Servlet 2.5 and JSP 2.1 Java API classes
 Apache Tomcat implements the Java Servlet and the JavaServer Pages (JSP)
 specifications from Sun Microsystems, and provides a "pure Java" HTTP web
 server environment for Java code to run.
 .
 This package contains the Java Servlet and JSP library.

Package: libservlet2.5-java-doc
Section: doc
Architecture: all
Depends: ${misc:Depends}
Description: Servlet 2.5 and JSP 2.1 Java API documentation
 Apache Tomcat implements the Java Servlet and the JavaServer Pages (JSP)
 specifications from Sun Microsystems, and provides a "pure Java" HTTP web
 server environment for Java code to run.
 .
 This package contains the documentation for the Java Servlet and JSP library.

#Package: tomcat6-admin
#Architecture: all
#Depends: tomcat6-common (>= ${source:Version}), ${misc:Depends}
#Description: Servlet and JSP engine -- admin web applications
# Apache Tomcat implements the Java Servlet and the JavaServer Pages (JSP)
# specifications from Sun Microsystems, and provides a "pure Java" HTTP web
# server environment for Java code to run.
# .
# This package contains the administrative web interfaces.

#Package: tomcat6-examples
#Architecture: all
#Depends: tomcat6-common (>= ${source:Version}), ${misc:Depends}
#Description: Servlet and JSP engine -- example web applications
# Apache Tomcat implements the Java Servlet and the JavaServer Pages (JSP)
# specifications from Sun Microsystems, and provides a "pure Java" HTTP web
# server environment for Java code to run.
# .
# This package contains the default Tomcat example webapps.

#Package: tomcat6-docs
#Section: doc
#Architecture: all
#Depends: tomcat6-common (>= ${source:Version}), ${misc:Depends}
#Description: Servlet and JSP engine -- documentation
# Apache Tomcat implements the Java Servlet and the JavaServer Pages (JSP)
# specifications from Sun Microsystems, and provides a "pure Java" HTTP web
# server environment for Java code to run.
# .
# This package contains the online documentation web application.

#Package: tomcat6-extras
#Architecture: all
#Depends: tomcat6-common (>= ${source:Version}), ${misc:Depends}
#Description: Servlet and JSP engine -- additional components
# Apache Tomcat implements the Java Servlet and the JavaServer Pages (JSP)
# specifications from Sun Microsystems, and provides a "pure Java" HTTP web
# server environment for Java code to run.
# .
# This package contains additional ("extra") component libraries.
# (Currently only catalina-jmx-remote.jar.)
